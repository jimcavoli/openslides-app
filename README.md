# OpenSlides Cloudron App Packaging

* OpenSlides server runs on port 8000, via supervisord from /app/server
  * Redis is available, so the server runs in "Big Mode"
  * Django channels and the collection cache both go in db 0 (default) prefixed, since they can't be configured to use another db
  * Sessions are in Redis db 1
  * The Django `SECRET_KEY` is read from `/app/data/.secret_key` which should contain just the text of the key, and no trailing newline. This is generated if not present by `/app/pkg/start.sh`
* OpenSlides client is served by nginx directly from /app/client
* The media service runs on port 8008, via supervisord from /app/media

## Customizations

* Some settings are configurable in `/app/data/settings.py`
  * `TIME_ZONE` is a good one to set to whatever time zone you would like the server to use. It's `America/New_York` by default
  * `ENABLE_ELECTRONIC_VOTING` is `True` by default, but you can completely disable the ability to vote electronically (and just have the `analog` manual-entry method) by changing the value to `False`
  * `ENABLE_SAML`
    * Actually using SAML option requires some additional setup, as noted in `settings.py`
    * This has not been tested yet, but all the dependencies and config files are in place as they should be
