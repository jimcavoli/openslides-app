import os
# Load openslides defaults
from openslides.global_settings import *
# Load any user-custom settings from settings.py
from settings import *

# Begin writing the Cloudron-forced settings last now - anything here may override user settings,
# as well as default ones, but this order ensures the app package isn't nearly as easily break-able.

SECRET_KEY = open("/app/data/.secret_key", "r").read().strip()

# The directory for user specific data files
OPENSLIDES_USER_DATA_DIR = '/app/data/openslides'

# Set the instance URL (full URL)
INSTANCE_DOMAIN = os.environ['CLOUDRON_APP_ORIGIN']

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ['CLOUDRON_POSTGRESQL_DATABASE'],
        'USER': os.environ['CLOUDRON_POSTGRESQL_USERNAME'],
        'PASSWORD': os.environ['CLOUDRON_POSTGRESQL_PASSWORD'],
        'HOST': os.environ['CLOUDRON_POSTGRESQL_HOST'],
        'PORT': os.environ['CLOUDRON_POSTGRESQL_PORT'],
    },
    'mediafiles': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ['CLOUDRON_POSTGRESQL_DATABASE'],
        'USER': os.environ['CLOUDRON_POSTGRESQL_USERNAME'],
        'PASSWORD': os.environ['CLOUDRON_POSTGRESQL_PASSWORD'],
        'HOST': os.environ['CLOUDRON_POSTGRESQL_HOST'],
        'PORT': os.environ['CLOUDRON_POSTGRESQL_PORT'],
    }
}

# Email settings
DEFAULT_FROM_EMAIL = os.environ['CLOUDRON_MAIL_FROM']
EMAIL_HOST = os.environ['CLOUDRON_MAIL_SMTP_SERVER']
EMAIL_HOST_PASSWORD = os.environ['CLOUDRON_MAIL_SMTP_PASSWORD']
EMAIL_HOST_USER = os.environ['CLOUDRON_MAIL_SMTP_USERNAME']
EMAIL_PORT = int(os.environ['CLOUDRON_MAIL_SMTP_PORT'])
EMAIL_USE_SSL = False
EMAIL_USE_TLS = False

# Django Channels
# https://channels.readthedocs.io/en/latest/topics/channel_layers.html#configuration
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [(os.environ['CLOUDRON_REDIS_HOST'], int(os.environ['CLOUDRON_REDIS_PORT']))],
            "capacity": 100000,
        },
    },
}

# Collection Cache
REDIS_ADDRESS = os.environ['CLOUDRON_REDIS_URL']
AMOUNT_REPLICAS = 1

# Session backend
# https://github.com/martinrusev/django-redis-sessions

SESSION_ENGINE = 'redis_sessions.session'
SESSION_REDIS = {
    'host': os.environ['CLOUDRON_REDIS_HOST'],
    'port': int(os.environ['CLOUDRON_REDIS_PORT']),
    'db': 1,
    'prefix': 'session',
    'socket_timeout': 2
}

# Make the SAML component available if the user enabled it in settings
if ENABLE_SAML:
  INSTALLED_APPS += ['openslides.saml']
  SETTINGS_FILEPATH = '/app/data/saml_settings.json'
